const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
  entry: './src/main.js',
  output: {
    path: __dirname + '../../../../wwwroot/js',
    filename: 'site.js',
    publicPath: '/',
    library: '[name]'
  },
  module: {
    rules: [{
        test: /\.(sa|sc|c)ss$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      }, {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      }, {
        test: /\.stylus$/,
        use: ['style-loader', 'css-loader', 'stylus-loader']
      },
      {
        test: /\.js$/,
        loader: 'babel-loader'
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    }),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  mode: 'production',
  watch: true,
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: 'source-map'
}

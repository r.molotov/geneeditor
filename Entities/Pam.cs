﻿using System;
using System.ComponentModel.DataAnnotations;

namespace geneeditor.Entities
{
    public class PAM
    {
        public int Id { get; set; }
        [StringLength(20)] public string Code { get; set; }

        public int ProtospacerId { get; set; }
        public Protospacer protospacer { get; set; }
        
        public int ProteinId { get; set; }
        public Protein Protein { get; set; }
    }
}

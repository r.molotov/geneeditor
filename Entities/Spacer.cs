﻿using System;
using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

namespace geneeditor.Entities
{
    public class Spacer
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public int BacId { get; set; }
        public Bac Bac { get; set; }

        public ICollection<Protospacer> Protospacers { get; set; }
    }
}
